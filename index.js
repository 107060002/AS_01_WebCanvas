var isProcessing = false;
var process = 'default';
var canvas;
var ctx;
var undo_buffer = [];
var redo_buffer = [];
var startX;
var startY;
var fillGraph = 0;
var undo;
var redo;

window.addEventListener('load', start);

function start() {
    const canvas = document.getElementById("canvas");
    const ctx = canvas.getContext("2d");

    canvas.addEventListener('mousedown', (e) => {
        console.log(process);
        isProcessing = true;
        if (process != 'default') {
            undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            redo_buffer = [];
        }
        ctx.globalCompositeOperation = "source-over";
        if (process === 'brush') {
            ctx.strokeStyle = document.getElementById("color").value;
            ctx.lineWidth = document.getElementById("brushSize").value;
            ctx.lineCap = 'round';
            ctx.beginPath();
            ctx.moveTo(e.offsetX, e.offsetY);
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();
        } else if (process === 'eraser') {
            //var radius = document.getElementById("brushSize").value / 2;
            ctx.globalCompositeOperation = "destination-out";
            ctx.lineWidth = document.getElementById("brushSize").value;
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();
            ctx.beginPath();
            ctx.moveTo(e.offsetX, e.offsetY);
        } else if (process === 'line') {
            ctx.strokeStyle = document.getElementById("color").value;
            ctx.lineWidth = document.getElementById("brushSize").value;
            ctx.lineCap = 'round';
            startX = e.offsetX;
            startY = e.offsetY;
        } else if (process === 'triangle') {
            ctx.strokeStyle = document.getElementById("color").value;
            ctx.fillStyle = document.getElementById("color").value;
            ctx.lineWidth = document.getElementById("brushSize").value;
            ctx.lineCap = 'round';
            startX = e.offsetX;
            startY = e.offsetY;
        } else if (process === 'rectangle') {
            ctx.strokeStyle = document.getElementById("color").value;
            ctx.fillStyle = document.getElementById("color").value;
            ctx.lineWidth = document.getElementById("brushSize").value;
            ctx.lineCap = 'round';
            startX = e.offsetX;
            startY = e.offsetY;
        } else if (process === 'circle') {
            ctx.strokeStyle = document.getElementById("color").value;
            ctx.fillStyle = document.getElementById("color").value;
            ctx.lineWidth = document.getElementById("brushSize").value;
            ctx.lineCap = 'round';
            startX = e.offsetX;
            startY = e.offsetY;
        } else if (process === 'type') {
            var _ = document.querySelector.bind(document);
            var _input = _("#canvasInput");
            var tool = {};

            tool.lineWidth = 20;
            tool.startx = 0;
            tool.starty = 0;

            _("#canvas").addEventListener('click', function (e) {
                if (process === 'type') {
                    tool.startx = e.pageX;
                    tool.starty = e.pageY;
                    _input.style.display = "block";
                    _input.style.position = "absolute";
                    _input.style.left = tool.startx + "px";
                    _input.style.top = (tool.starty - tool.lineWidth) + "px";
                    _input.focus();
                }
            });

            _input.addEventListener('keyup', function (e) {
                //Pressing enter to put the text to the canvas
                if (e.which === 13) {
                    e.preventDefault();
                    ctx.font = document.getElementById("fontSize").value + "px " + document.getElementById("fontType").value;
                    ctx.fillStyle = document.getElementById("color").value;;
                    ctx.fillText(_input.value, tool.startx - canvas.offsetLeft, tool.starty - canvas.offsetTop + (tool.lineWidth / 2));
                    ctx.save();
                    _input.style.display = "none";
                    _input.value = "";
                }
                //Pressing Escape to cancel
                if (e.which === 27) {
                    e.preventDefault();
                    _input.style.display = "none";
                    _input.value = "";
                }
            }, false);
        }
    });

    canvas.addEventListener('mouseup', () => {
        isProcessing = false;
        ctx.beginPath();
    });

    canvas.addEventListener('mousemove', (e) => {
        if (isProcessing) {
            ctx.globalCompositeOperation = "source-over";
            if (process === 'brush') {
                ctx.lineCap = 'round';
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.stroke();
                ctx.beginPath();
                ctx.moveTo(e.offsetX, e.offsetY);
            } else if (process === 'line') {
                var temp;
                ctx.putImageData(temp = undo_buffer.pop(), 0, 0);
                undo_buffer.push(temp);
                ctx.lineCap = 'round';
                ctx.beginPath();
                ctx.moveTo(startX, startY);
                ctx.lineTo(e.offsetX, e.offsetY);

                ctx.stroke();
            } else if (process === 'eraser') {
                //var radius = document.getElementById("brushSize").value / 2;
                ctx.globalCompositeOperation = "destination-out";
                ctx.lineWidth = document.getElementById("brushSize").value;
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.stroke();
                ctx.beginPath();
                ctx.moveTo(e.offsetX, e.offsetY);
            } else if (process === 'triangle') {
                var temp;
                ctx.putImageData(temp = undo_buffer.pop(), 0, 0);
                undo_buffer.push(temp);
                ctx.beginPath();
                ctx.moveTo(startX, startY);
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.lineTo(startX - (e.offsetX - startX), e.offsetY);
                ctx.closePath();
                if (!fillGraph) {
                    ctx.stroke();
                } else {
                    ctx.fill();
                }
            } else if (process === 'rectangle') {
                var temp;
                ctx.putImageData(temp = undo_buffer.pop(), 0, 0);
                undo_buffer.push(temp);
                ctx.beginPath();
                ctx.moveTo(startX, startY);
                ctx.lineTo(startX, e.offsetY);
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.lineTo(e.offsetX, startY);
                ctx.closePath();
                if (!fillGraph) {
                    ctx.stroke();
                } else {
                    ctx.fill();
                }
            } else if (process === 'circle') {
                var temp;
                var centerX = (e.offsetX + startX) / 2;
                var centerY = (e.offsetY + startY) / 2;
                ctx.putImageData(temp = undo_buffer.pop(), 0, 0);
                undo_buffer.push(temp);
                ctx.beginPath();
                ctx.arc(centerX, centerY, Math.sqrt(Math.pow(e.offsetX - centerX, 2) + Math.pow(e.offsetY - centerY, 2)), 0, 2 * Math.PI);
                if (!fillGraph) {
                    ctx.stroke();
                } else {
                    ctx.fill();
                }
            }
        }
    });
    undo = function () {
        if (undo_buffer.length > 0) {
            redo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            ctx.putImageData(undo_buffer.pop(), 0, 0);
        }
        process = 'default';
        document.getElementsByTagName("body")[0].style.cursor = 'default';
    }

    redo = function () {
        if (redo_buffer.length > 0) {
            undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            ctx.putImageData(redo_buffer.pop(), 0, 0);
        }
        process = 'default';
        document.getElementsByTagName("body")[0].style.cursor = 'default';
    }

}

function chooseTool(name) {
    process = name;
    console.log(name);
    if (name === 'brush') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/brush_cur.png), auto";
    } else if (name === 'eraser') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/eraser_cur.png), auto";
    } else if (name === 'rectangle') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/rectangle_cur.png), auto";
    } else if (name === 'triangle') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/triangle_cur.png), auto";
    } else if (name === 'circle') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/circle_cur.png), auto";
    } else if (name === 'line') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/line_cur.png), auto";
    } else if (name === 'type') {
        document.getElementsByTagName("body")[0].style.cursor = "url(img/type_cur.png), auto";
    }
}

function reset() {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function fill() {
    var box = document.getElementById("checkbox");
    if (box.checked) {
        fillGraph = 1;
    } else {
        fillGraph = 0;
    }
}

function _upload() {
    document.getElementById('upload').click();
}

function upload(e) {
    const drawable = document.getElementById("canvas");
    const ctx = drawable.getContext("2d");
    let file = new FileReader();
    file.readAsDataURL(e.target.files[0]);
    file.onload = (e) => {
        var img = new Image();
        img.src = e.target.result;
        img.onload = () => {
            ctx.drawImage(img, 100, 100, img.width, img.height);
        }
        undo_buffer.push(ctx.getImageData(0, 0, drawable.width, drawable.height));
    }
}

function download() {
    var link = document.createElement('a');
    link.download = 'goodjob.png';
    link.href = document.getElementById('canvas').toDataURL()
    link.click();
}

