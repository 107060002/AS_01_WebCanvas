# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Fill the graph you draw                                  | 1~5%     | Ｙ         |


---

### How to use 

![](https://i.imgur.com/sB5gTrt.png)

    The picture above is the appearance of my webpage.
    
    You can draw on the white area, and choose tools form the table 
    on righthand side by clicking the icons.
    
    Now we talk about what function these icon represent. 

> Tools
* Icons
    * Brush   ![](https://i.imgur.com/t72BXBg.png)
    You can use a brush to draw arbitary lines.
    * Eraser  ![](https://i.imgur.com/1W1iCcu.png)
    You can use an eraser to clean the canvas, it erases things like a brush.
    * Line   ![](https://i.imgur.com/JwALnKV.png)
    You can use it to draw straight lines.
    * Triangle ![](https://i.imgur.com/9E61BXr.png)
    You can drag on the canvas to draw an triangle, the size is diceded by yourself.
    * Rectangle  ![](https://i.imgur.com/5McCoyx.png)
    You can drag on the canvas to draw an rectangle, the size is diceded by yourself.
    * Circle ![](https://i.imgur.com/Y2bH9VQ.png)
    You can drag on the canvas to draw an circle, the size is diceded by yourself.
    * Type text ![](https://i.imgur.com/UtDWRkl.png)
    Click on the canvas, and you can start typing. After finishing typing, you  shuold
    press enter to have the words shown on the canvas.
    * Upload  ![](https://i.imgur.com/4qrdglx.png)
    You can upload your local image and show on the canvas.
    * Download ![](https://i.imgur.com/rrMMaiS.png)
    You can download what you draw on the canvas as an .png file.
    * Undo ![](https://i.imgur.com/qg9QKPx.png)
    If you click this icon, you can delete what you draw on the last step.
    * Redo ![](https://i.imgur.com/vm2H2x2.png)
    If you click this icon, you can call what you use "Undo" to delete back.
    * Reset ![](https://i.imgur.com/TecrvfH.png)
    If you click this icon, all things you have drawn will be cleaned.
* Menu
    * Select color
        If you click the box beside "Select color", you can select a color, and what you draw later will be in this color.
    * Brush size
        You can drag the bar beside "Brush size" to determine the thickness of what you draw.
    * Font type
        You can change the font type, and what you type will be in the font type.
    * Font size
        You can change the font size, and what you type will be in the font size.


### Function description

* Fill your graph

    When the checkbox beside "Fill your graph" is checked, the rectangle, triangle and circle you draw will be filled.

### Gitlab page link

    https://107060002.gitlab.io/AS_01_WebCanvas


<style>
table th{
    width: 100%;
}
</style>